var seneca = require('seneca')();

seneca.add({role: 'shipping', cmd: 'getById'}, function(msg, respond) {
  var shipping = {
    shipping: 3.99
  };

  respond(null, shipping);
});

seneca.listen();

seneca.ready(function() {
  console.log('shipping-service ready!');
});
